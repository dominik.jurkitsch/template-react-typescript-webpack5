import React from "react";
import ReactDOM from "react-dom";

const App = () => (
  <h1>Start here with your new React, TypeScript, Webpack 5 Application!</h1>
);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
